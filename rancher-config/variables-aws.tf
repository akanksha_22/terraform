#
# Variables for AWS related configuration
#

variable "aws_access_key" {
  type        = string
  description = "AWS access key"
}

variable "aws_secret_key" {
  type        = string
  description = "AWS API secret access key"
}

variable "aws_ami_id" {
  type        = string
  description = "AWS AMI ID to use for instance"
  default     = "ami-b9daeddc"
}

variable "aws_ami_user" {
  type        = string
  description = "Username with SSH access for the specified AWS AMI"
  default     = "ami-b9daeddc"
}

variable "aws_region" {
  type        = string
  description = "AWS region to use for K8s nodes"
  default     = "us-east-2"
}

variable "aws_vpc_id" {
  type        = string
  description = "AWS VPC to use for K8s nodes"
  default     = "vpc-00bfd069"
}

variable "aws_security_groups" {
  type        = list(string)
  description = "List of AWS security group (names) to attach"
  default     = ["HE3-Corp-Allow"]
}

variable "aws_subnet_id" {
  type        = string
  description = "AWS subnet ID to use for instance"
  default     = "subnet-3c702871"
}

variable "aws_instance_tags" {
  type        = string
  description = "Comma separated key,value of tags for EC2 instance"
  default     = "Owner,DevOps,CostCenter,CC100"
}

variable "aws_iam_role" {
  type = object({
    master = string
    worker = string
  })
  description = "Name of AWS IAM role to attach to EC2 instance"

  default = {
    master = "Rancher-2-controlplane-etcd"
    worker = "Rancher-2-worker-nodes"
  }
}

variable "aws_availability_zone" {
  type        = string
  description = "Availability zone (a,b,c,d,e) under the specified subnet"
  default     = "c"
}

variable "aws_ec2_disk_size" {
  type = object({
    master = string
    worker = string
  })
  description = "Size of the root disk (in GB) for EC2 instance"

  default = {
    master = "50"
    worker = "100"
  }
}

variable "aws_ec2_instance_type" {
  type = object({
    master = string
    worker = string
  })
  description = "AWS instance type for Kubernetes master and worker nodes"

  default = {
    master = "t2.medium"
    worker = "t2.xlarge"
  }
}