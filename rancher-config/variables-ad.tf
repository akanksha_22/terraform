#
# Variables for Active Directory authentication
#

variable "ad_servers" {
  type        = list(string)
  description = "Active Directory servers to be used for authentication"
  default     = ["pdcprodinfdc01.headquarters.healthedge.com"]
}

variable "ad_server_port" {
  type        = number
  description = "Service account username for Active Directory integration"
  default     = 389
}

variable "ad_login_domain" {
  type        = string
  description = "Default login domain on Active Directory"
  default     = "headquarters"
}

variable "ad_svc_user_name" {
  type        = string
  description = "Service account username for Active Directory integration"
}

variable "ad_svc_user_dn" {
  type        = string
  description = "Distinguished Name of service account in Active Directory"
}

variable "ad_svc_passwd" {
  type        = string
  description = "Service account password for Active Directory integration"
}

variable "ad_tls_enabled" {
  type        = bool
  description = "Service account passwo for Active Directory integration"
  default     = false
}

variable "ad_search_base" {
  type        = string
  description = "Active Directory search base"
  default     = "DC=headquarters,DC=healthedge,DC=com"
}

variable "ad_users_acl" {
  type        = string
  description = "Active Directory group ACL for Kubernetes users"
  default     = "ACL-Kubernetes-User"
}

variable "ad_admins_acl" {
  type        = string
  description = "Active Directory ACL for Kubernetes users"
  default     = "ACL-Kubernetes-Admin"
}