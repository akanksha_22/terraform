# Create Node Templates for K8s nodes in AWS

resource "rancher2_node_template" "aws_master" {
  provider = rancher2.admin

  name                     = "aws-master"
  description              = "Node template for master nodes of K8s clusters in AWS"
  cloud_credential_id      = rancher2_cloud_credential.he_aws.id
  engine_insecure_registry = var.insecure_registry

  amazonec2_config {
    ami                  = var.aws_ami_id
    region               = var.aws_region
    root_size            = var.aws_ec2_disk_size.master
    instance_type        = var.aws_ec2_instance_type.master
    iam_instance_profile = var.aws_iam_role.master
    security_group       = var.aws_security_groups
    subnet_id            = var.aws_subnet_id
    vpc_id               = var.aws_vpc_id
    zone                 = var.aws_availability_zone
    private_address_only = true
    use_private_address  = true
    tags                 = var.aws_instance_tags
  }
}

resource "rancher2_node_template" "aws_worker" {
  provider = rancher2.admin

  name                     = "aws-worker"
  description              = "Node template for worker nodes of K8s clusters in AWS"
  cloud_credential_id      = rancher2_cloud_credential.he_aws.id
  engine_insecure_registry = var.insecure_registry

  amazonec2_config {
    ami                  = var.aws_ami_id
    region               = var.aws_region
    root_size            = var.aws_ec2_disk_size.worker
    instance_type        = var.aws_ec2_instance_type.worker
    iam_instance_profile = var.aws_iam_role.worker
    security_group       = var.aws_security_groups
    subnet_id            = var.aws_subnet_id
    vpc_id               = var.aws_vpc_id
    zone                 = var.aws_availability_zone
    private_address_only = true
    use_private_address  = true
    tags                 = var.aws_instance_tags
  }
}

# Create node Templates for K8s nodes in vSphere vRA

resource "rancher2_node_template" "vsphere_small" {
  provider = rancher2.admin

  name                     = "vsphere-small"
  description              = "Node template (small) for K8s cluster nodes in vSphere"
  cloud_credential_id      = rancher2_cloud_credential.he_vsphere.id
  engine_insecure_registry = var.insecure_registry

  vsphere_config {
    creation_type = "template"
    clone_from    = var.vsphere_source_template
    datacenter    = var.vsphere_datacenter
    pool          = var.vsphere_resource_pool
    datastore     = var.vsphere_datastore
    folder        = var.vsphere_folder
    cpu_count     = var.vsphere_vm_cpu_count.small
    memory_size   = var.vsphere_vm_memory.small
    disk_size     = var.vsphere_vm_disksize.small
    network       = var.vsphere_network
    cfgparam      = ["disk.enableUUID=TRUE"]
  }
}

resource "rancher2_node_template" "vsphere_medium" {
  provider = rancher2.admin

  name                     = "vsphere-medium"
  description              = "Node template (medium) for K8s cluster nodes in vSphere"
  cloud_credential_id      = rancher2_cloud_credential.he_vsphere.id
  engine_insecure_registry = var.insecure_registry

  vsphere_config {
    creation_type = "template"
    clone_from    = var.vsphere_source_template
    datacenter    = var.vsphere_datacenter
    pool          = var.vsphere_resource_pool
    datastore     = var.vsphere_datastore
    folder        = var.vsphere_folder
    cpu_count     = var.vsphere_vm_cpu_count.medium
    memory_size   = var.vsphere_vm_memory.medium
    disk_size     = var.vsphere_vm_disksize.medium
    network       = var.vsphere_network
    cfgparam      = ["disk.enableUUID=TRUE"]
  }
}

resource "rancher2_node_template" "vsphere_large" {
  provider = rancher2.admin

  name                     = "vsphere-large"
  description              = "Node template (large) for K8s cluster nodes in vSphere"
  cloud_credential_id      = rancher2_cloud_credential.he_vsphere.id
  engine_insecure_registry = var.insecure_registry

  vsphere_config {
    creation_type = "template"
    clone_from    = var.vsphere_source_template
    datacenter    = var.vsphere_datacenter
    pool          = var.vsphere_resource_pool
    datastore     = var.vsphere_datastore
    folder        = var.vsphere_folder
    cpu_count     = var.vsphere_vm_cpu_count.large
    memory_size   = var.vsphere_vm_memory.large
    disk_size     = var.vsphere_vm_disksize.large
    network       = var.vsphere_network
    cfgparam      = ["disk.enableUUID=TRUE"]
  }
}