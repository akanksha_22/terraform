# Terraform provider configuration for Rancher

provider "rancher2" {
  alias = "bootstrap"

  api_url   = var.rancher_url
  bootstrap = true
  insecure  = true
}

# Create a new rancher2_bootstrap using bootstrap provider config
resource "rancher2_bootstrap" "admin" {
  provider = rancher2.bootstrap

  password  = var.rancher_admin_password
  telemetry = true
}

# Provider config for admin
provider "rancher2" {
  alias = "admin"

  api_url   = rancher2_bootstrap.admin.url
  token_key = rancher2_bootstrap.admin.token
  insecure  = true
}