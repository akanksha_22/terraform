# Configure Active Directory authentication for Rancher

resource "rancher2_auth_config_activedirectory" "activedirectory" {
  provider = rancher2.admin

  servers                  = var.ad_servers
  service_account_username = var.ad_svc_user_name
  service_account_password = var.ad_svc_passwd
  user_search_base         = var.ad_search_base
  default_login_domain     = var.ad_login_domain
  port                     = var.ad_server_port
  test_username            = var.ad_svc_user_name
  test_password            = var.ad_svc_passwd
  tls                      = var.ad_tls_enabled
  access_mode              = var.rancher_access_mode

  allowed_principal_ids = [
    "activedirectory_user://CN=${var.ad_svc_user_dn},OU=HE-SVC-Accounts,${var.ad_search_base}",
    "activedirectory_group://CN=${var.ad_users_acl},OU=Groups,OU=HE-Users,${var.ad_search_base}",
    "activedirectory_group://CN=${var.ad_admins_acl},OU=Groups,OU=HE-Users,${var.ad_search_base}"
  ]
}

resource "rancher2_global_role_binding" "ad_users" {
  provider = rancher2.admin

  name               = "ad-users"
  global_role_id     = "user"
  group_principal_id = "activedirectory_group://CN=${var.ad_users_acl},OU=Groups,OU=HE-Users,${var.ad_search_base}"
}

resource "rancher2_global_role_binding" "ad_admins" {
  provider = rancher2.admin

  name               = "ad-admins"
  global_role_id     = "admin"
  group_principal_id = "activedirectory_group://CN=${var.ad_admins_acl},OU=Groups,OU=HE-Users,${var.ad_search_base}"
}