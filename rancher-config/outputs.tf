output "rancher_config_url" {
  value       = rancher2_bootstrap.admin.url
  description = "The URL of Rancher configured"
}

output "rancher_admin_token" {
  value       = rancher2_bootstrap.admin.token
  description = "The API token used for Rancher configuration"
}

output "aws_master_node_template" {
  value       = rancher2_node_template.aws_master.id
  description = "The id of node template for K8s master nodes in AWS"
}

output "aws_worker_node_template" {
  value       = rancher2_node_template.aws_worker.id
  description = "The id of node template for K8s worker nodes in AWS"
}

output "vsphere_small_node_template" {
  value       = rancher2_node_template.vsphere_small.id
  description = "The id of node template for K8s nodes in vSphere with small configuration"
}

output "vsphere_medium_node_template" {
  value       = rancher2_node_template.vsphere_medium.id
  description = "The id of node template for K8s nodes in vSphere with medium configuration"
}

output "vsphere_large_node_template" {
  value       = rancher2_node_template.vsphere_large.id
  description = "The id of node template for K8s nodes in vSphere with large configuration"
}