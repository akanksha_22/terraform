#
# Variables for Rancher configuration
#

variable "rancher_url" {
  type        = string
  description = "The Rancher URL - to perform API operations"
}

variable "rancher_admin_password" {
  type        = string
  description = "Password to use for the Rancher admin user"
}

variable "rancher_access_mode" {
  type        = string
  description = "Access mode for Rancher"
  default     = "restricted"
}

variable "insecure_registry" {
  type        = list(string)
  description = "Insecure registry details in host:port format for K8s nodes"
  default     = ["heeng-k8snexus.headquarters.healthedge.com:8123"]
}