# Create cloud credentials for AWS and vSphere vRA

resource "rancher2_cloud_credential" "he_aws" {
  provider = rancher2.admin

  name        = "he-aws-creds"
  description = "HealthEdge credentials for AWS"

  amazonec2_credential_config {
    access_key = var.aws_access_key
    secret_key = var.aws_secret_key
  }
}

resource "rancher2_cloud_credential" "he_vsphere" {
  provider = rancher2.admin

  name        = "he-vsphere-creds"
  description = "HealthEdge credentials for vSphere"

  vsphere_credential_config {
    vcenter      = var.vsphere_server
    vcenter_port = var.vsphere_port
    username     = var.vsphere_user
    password     = var.vsphere_password
  }
}

